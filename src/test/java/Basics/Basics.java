package Basics;

import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;

public class Basics {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Validating add place API as expected
			//Given - All input methods
			//when - Submit the api there
			//Then - validate the response
		
		RestAssured.baseURI="https://rahulshettyacademy.com";
		given().log().all().queryParam("Key","qaclick123").header("Content-Type","application/json").body("{\r\n"
				+ "	\"location\":{\r\n"
				+ "		\"lat\":19�44'09.5\r\n"
				+ "		\"lng\":77�08'34.9\r\n"
				+ "	},\r\n"
				+ "	\"accuracy\"=0,\r\n"
				+ "	\"name\"=\"Shubham Bahalaskar\",\r\n"
				+ "	\"phone_Number\"=(+91) 7030620961,\r\n"
				+ "	\"address\":\"29, near gayatri temple, gayatri nagar, Hingoli\",\r\n"
				+ "	\"types\":[\"Home\"],\r\n"
				+ "	\"website\": \"NA\",\r\n"
				+ "	\"language\"=\"Marathi\"\r\n"
				+ "}").when().post("maps/api/place/add/json").then().log().all().assertThat().statusCode(200);
	}

}
